{
  buildFruitLike,
  fetchFromGitHub,
}:
buildFruitLike {
  pname = "yuzu";
  version = "1734";

  src = fetchFromGitHub {
    owner = "yuzu-mirror";
    repo = "yuzu-mainline";
    rev = "a9d53b735deb253a45e6a64ae875fe02a09d1d7e";
    hash = "sha256-6g6JxujKfvCs0oPf4kNZ/muUyjiS5vQCVogxftqG03o=";
    fetchSubmodules = true;
  };

  meta = {
    homepage = "https://yuzu-emu.org";
    mainProgram = "yuzu";    
  };
}
