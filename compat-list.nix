{
  stdenv,
  src,
}:
stdenv.mkDerivation {
  pname = "yuzu-compatibility-list";
  version = "unstable-${src.lastModifiedDate}";
  inherit src;

  buildCommand = ''
    cp $src/compatibility_list.json $out
  '';
}
