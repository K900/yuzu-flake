{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    compat-list-src = {
      url = "github:flathub/org.yuzu_emu.yuzu";
      flake = false;
    };

    sudachi-src = {
      url = "git+https://github.com/sudachi-emu/sudachi.git?submodules=1";
      flake = false;
    };

    suyu-git-src = {
      url = "git+https://git.suyu.dev/suyu/suyu.git?submodules=1";
      flake = false;
    };
  };

  outputs = { nixpkgs, ... }@inputs: let
    inherit (nixpkgs) lib;
    forAllSystems = lib.genAttrs ["x86_64-linux" "aarch64-linux"];
  in {
    packages = forAllSystems (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in
      pkgs.lib.makeScope pkgs.qt6Packages.newScope (self: {
        buildFruitLike = self.callPackage ./lib/build-fruit-like.nix {};

        # Dependencies
        compat-list = self.callPackage ./compat-list.nix {
          src = inputs.compat-list-src;
        };
        nx_tzdb = self.callPackage ./nx_tzdb.nix {};

        # Original Yuzu code
        mainline = self.callPackage ./mainline.nix {};
        early-access = self.callPackage ./early-access {};

        # Forks
        sudachi = self.callPackage ./sudachi {
          src = inputs.sudachi-src;
        };
        suyu = self.callPackage ./suyu.nix {};
        suyu-git = self.callPackage ./suyu-git.nix {
          src = inputs.suyu-git-src;
        };

        default = self.mainline;
      }));
  };
}
