{
  buildFruitLike,
  mainline,
  fetchzip,
  fetchgit,
  runCommand,
  git,
  gnutar,
}:
# The mirror repo for early access builds is missing submodule info,
# but the Windows distributions include a source tarball, which in turn
# includes the full git metadata. So, grab that and rehydrate it.
# This has the unfortunate side effect of requiring two FODs, one
# for the Windows download and one for the full repo with submodules.
let
  sources = import ./sources.nix;

  zip = fetchzip {
    name = "yuzu-ea-windows-dist";
    url = "https://github.com/pineappleEA/pineapple-src/releases/download/EA-${sources.version}/Windows-Yuzu-EA-${sources.version}.zip";
    hash = sources.distHash;
  };

  gitSrc =
    runCommand "yuzu-ea-dist-unpacked" {
      src = zip;
      nativeBuildInputs = [git gnutar];
    }
    ''
      mkdir $out
      tar xf $src/*.tar.xz --directory=$out --strip-components=1
      cd $out
      git submodule set-url externals/breakpad https://github.com/yuzu-mirror/breakpad.git
      git submodule set-url externals/discord-rpc https://github.com/yuzu-mirror/discord-rpc.git
      git submodule set-url externals/dynarmic https://github.com/yuzu-mirror/dynarmic.git
      git submodule set-url externals/mbedtls https://github.com/yuzu-mirror/mbedtls.git
      git submodule set-url externals/sirit https://github.com/yuzu-mirror/sirit.git
      git config user.name "automated"
      git config user.email "automated@example.com"
      git commit .gitmodules -m "redirect submodules"
    '';

  rehydratedSrc = fetchgit {
    name = "yuzu-ea-rehydrated";
    url = gitSrc;
    fetchSubmodules = true;
    hash = sources.fullHash;
  };
in
  buildFruitLike {
    pname = "yuzu-early-access";
    version = sources.version;
    src = rehydratedSrc;
    passthru.updateScript = ./update.sh;
    meta = mainline.meta // {description = mainline.meta.description + " - early access branch";};
  }
