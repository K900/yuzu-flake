{
  buildFruitLike,
  src,
}:
buildFruitLike {
  pname = "suyu-git";
  version = "unstable-${src.lastModifiedDate}";
  inherit src;

  cmakeOptionPrefix = "SUYU_";

  meta = {
    homepage = "https://suyu.dev/";
    mainProgram = "suyu";
  };
}
