{
  buildFruitLike,
  src,
}:
buildFruitLike {
  pname = "sudachi";
  version = "unstable-${src.lastModifiedDate}";

  inherit src;

  patches = [
    # revert cmakelists ordering changes
    ./cmake.patch
  ];

  cmakeOptionPrefix = "SUDACHI_";
  withTranslations = false;

  meta = {
    homepage = "https://github.com/sudachi-emu/sudachi";
    mainProgram = "sudachi";
  };
}
