{
  buildFruitLike,
  fetchFromGitea,
  nix-update-script,
}:
buildFruitLike rec {
  pname = "suyu";
  version = "0.0.3";

  src = fetchFromGitea {
    domain = "git.suyu.dev";
    owner = "suyu";
    repo = "suyu";
    rev = "v${version}";
    fetchSubmodules = true;
    hash = "sha256-wLUPNRDR22m34OcUSB1xHd+pT7/wx0pHYAZj6LnEN4g=";
  };

  cmakeOptionPrefix = "SUYU_";

  passthru.updateScript = nix-update-script {
    extraArgs = ["--version-regex" "v(.*)"];
  };

  meta = {
    homepage = "https://suyu.dev/";
    mainProgram = "suyu";
  };
}
